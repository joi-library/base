/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.elwinbran.javaoverhaul.base;

/**
 * A more maintainable and immutable variant of {@link Iterator}.
 * 
 * @author Elwin Slokker
 * @param <ElementT>
 */
public interface Traverser<ElementT>
{
    /**
     * 'Traverses' the elements and retrieves the next traverser.
     * @return The traversers that comes after this one as dictated by 
     * the rules of wherever the first Traverser came from. Or an empty wrapper
     * if there is no element after this one.
     */
    public abstract NullableWrapper<Traverser<ElementT>> traverse();
    
    /**
     * @return The element this traverser represents.
     */
    public abstract ElementT element();
}
