/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.base;


/**
 * An object that produces possible null-return result wrappers.
 * The factory provides null-return as well as normal not-null returns.
 * 
 * @author Elwin Slokker
 * @see NullableWrapper
 */
public interface NullableWrapperFactory
{
    /**
     * Produces a result wrapper object with a "{@code null}".
     * So the {@link NullableWrapper#isNil() isNil()} method of this object will
     * always return {@code true}.
     * 
     * @param <NullableT> The required result type.
     * @return An empty result wrapper.
     */
    public abstract <NullableT> NullableWrapper<NullableT> make();
    
    
    /**
     * Produces a result wrapper object that wraps a result.
     * So the {@link NullableWrapper#isNil() isNil()} method of this object will
     * always return {@code false} and {@link NullableWrapper#getWrapped() } 
     * returns the {@code result}.
     * 
     * @param <NullableT> The required result type and type of the given result.
     * @param result The actual result that could have been {@code null}.
     * NEVER PASS {@code null}! USE {@link #make() } INSTEAD!
     * @return A result wrapper containing the given result.
     */
    public abstract <NullableT> NullableWrapper<NullableT> make(NullableT result);
}
