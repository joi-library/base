/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.base;

/**
 * Represents a thing that can determine the order of two objects of the same 
 * type. Many things are comparable as long as there is a certain context. 
 * Comparating usually involves counting or calculating.
 * 
 * @author Elwin Slokker
 * @param <ComparableT> The type of the comparable objects.
 */
public interface NewComparator<ComparableT>
{
    /**
     * Compares the given objects and declares whether the first is greater than
     * the second. The order of parameters only matters when {@link 
     * #areEqual(java.lang.Object, java.lang.Object) areEqual()} with the same 
     * arguments is {@code false}.
     * Think of this method as evaluating "left > right".
     * 
     * @param left The object that the comparison is viewed from.
     * @param right The object that is compared with.
     * @return A special comparation result object that has to be intpereted 
     * as {@code left} {compare method} {@code right}.
     */
    public abstract Comparation compare(ComparableT left, ComparableT right);
}
