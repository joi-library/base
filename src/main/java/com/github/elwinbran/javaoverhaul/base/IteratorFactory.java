/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.base;

import java.util.Iterator;

/**
 * An object that produces a few types of {@link java.util.Iterator iterators}.
 * 
 * @author Elwin Slokker
 * @see Iterator
 */
public interface IteratorFactory
{
    /**
     * Produces an iterator that iterates nothing.
     * 
     * @param <ElementT> The required element type.
     * @return An iterator with {@link Iterator#hasNext()} {@code == false}.
     */
    public abstract <ElementT> Iterator<ElementT> makeEmpty();
    
    /**
     * Produces an iterator over a single given element.
     * 
     * @param <ElementT> The required element type.
     * @param element The element to be iterated.
     * @return An iterator that first return the only element and then nothing.
     */
    public abstract <ElementT> Iterator<ElementT> make(ElementT element);
}
