/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.base;

/**
 * An object used to wrap results that could be {@code null}.
 * This simply replaces the well-known 'null-check'.
 * When this object is the return type it also signals the method could return 
 * {@code null}s.
 * 
 * @author Elwin Slokker
 * @param <NullableT> The type of the possibly {@code null}-valued object.
 */
public interface NullableWrapper<NullableT>
{
    /**
     * Specifies whether the wrapped object is {@code null} or not.
     * 
     * @return {@code true} if {@link #getWrapped getWrapped()} returns a 
     * non-{@code null} object and {@code false} otherwise.
     */
    public abstract boolean isNil();
    
    /**
     * Extract result object from wrapper.
     * 
     * @return The result object if {@link #isNil isNill()}{@code == false};
     * {@code null} or an exception otherwise.
     */
    public abstract NullableT getWrapped();
}
