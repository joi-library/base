/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.base;

/**
 * Defines an object that can make a 'deep clone' of itself.
 * A deep clone is a copy of an object that has no references to mutable 
 * fields in the original object. This should be used when safe copies are
 * required of mutable objects.
 * NOTE: this class is theoretically useless, because within a well-designed 
 * system there are no mutable objects. Adding a cloning method to an object
 * also gives it a second responsibility, breaking S (Objects have
 * only a single responsibility) of SOLID.
 * 
 * @author Elwin Slokker
 * @param <CloneT> What type the clone is. It is advisable to use the highest 
 * class (interface or abstract class actually) in the hierarchy.
 */
public interface DeepCloneable<CloneT extends DeepCloneable>
{
    /**
     * Makes an exact clone of this object.
     * @return a copy of {@code this}.
     */
    public abstract CloneT deepClone();
}
