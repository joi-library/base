/**
 * 'Root' package of the base library of the JavaOverhaulInterface 
 * (super-)library.
 * <br>Root because all other packages/rely on this package.
 * <br>Contains a few extremely basic interfaces that prevent code duplication.
 * <list>
 * <li>Pair</li>
 * <li>DeepCloneable</li>
 * <li>NullableWrapper</li>
 * <li>Equivalator</li>
 * <li>Comparator</li>
 * <li>IteratorFactory</li>
 * <li>PairFactory</li>
 * <li>NullableWrapperFactory</li>
 * </list>
 * 
 * 
 * <h1>Contents</h1>
 * -{@link com.github.elwinbran.javaoverhaul.base.DeepCloneable}: A class for 
 * making mutable objects type safe cloneable.
 * <br>-{@link com.github.elwinbran.javaoverhaul.base.NullableWrapper}:
 * Replaces null-returns and null checks.
 * <br>-{@link com.github.elwinbran.javaoverhaul.base.Pair}: An improved 
 * immutable version of existing key-value storing objects.
 * 
 */
package com.github.elwinbran.javaoverhaul.base;