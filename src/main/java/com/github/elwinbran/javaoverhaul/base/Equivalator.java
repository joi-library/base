/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.base;

/**
 * Represents a thing that can determine whether to objects of the same type 
 * are equal. Objects should be equal if the behaviour they show is the same 
 * (same return results).
 * 
 * @author Elwin Slokker
 * @param <EquavalentT> The type of the equivable objects.
 */
public interface Equivalator<EquavalentT>
{
    /**
     * Checks whether the given objects are equal.
     * Order of arguments may not matter (equality is commutative).
     * 
     * @param first Simply the first object
     * @param second Simply the second object.
     * @return {@code true} if the objects show the same behaviour; 
     * {@code false} otherwise.
     */
    public abstract boolean areEqual(EquavalentT first, EquavalentT second);
}
