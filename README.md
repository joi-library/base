## Base ##

This interface library is a part of JOI (Java Interface Overhaul).
JOI along with a full implementation is a more maintainable substitute for the
JDK. 

This library in particular deals with highly reusable interfaces that form the
backbone of almost any program.
A notable feature is the introduction of Equivalators: these are meant to
specifically replace the Object.isEqual every OOP language seems to have.
Needless to be said, it is not allowed to use any Object method in JOI 
compliant code. 