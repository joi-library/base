Pull requests are to be judged individually by me. Think about the worth of the
request first, since domain-specific changes are likely to get rejected.
Instead of changing Base, you can branch it and make your own version (
which applies to the whole of JOI).

Whatever the reasons are for your pull request, when the contents are merged
I will apply the SemVer rules to it. If you use JOI 1.5.22 for example and
want something changed that causes a major version change (NullableWrapper is
changed) in that version, that will simply not happen. 
Your pull request content would appear in JOI 2.0.0 as earliest version, in 
this case.

Issues barely apply to this library, but I will fix issues that are applied as
patches and cannot guarantee that I take action based on any other issue.
In the latter case, please provide theoretical backing for the issue, in order
to be taken seriously.